cmake_minimum_required(VERSION 3.5)
include($ENV{IDF_PATH}/tools/cmake/project.cmake)
set(EXTRA_COMPONENT_DIRS third-party/esp-idf-lib/components
                         third-party/i2cdevlib/ESP32_ESP-IDF/components
)
project(free-fly-esp32)