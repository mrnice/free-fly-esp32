# Free Fly ESP32

This project aims to use the famous ESP32 to connect cheep flying sensors to Android
ebook readers without the need to modify the reader hardware. As most ebook readers
have Wifi chips build in. Because we have a bluetooth and a USB UART connection it should
be pretty simple to connect to a lot of devices including your favorite Smartphone.

For now this project uses the open vario protocol [1], but we aim to implement different protocols
to make XCTrack, XCsoar and common Applications happy.

1 : https://www.openvario.org/doku.php?id=projects:series_00:software:nmea

The sensors used in this project are:

GY-86 module (~15 Euro)

MS5611-01BA03 barometric pressure sensor (with ~10cm resolution)
MPU-6050 9-axis gyrometer
HMC5883L 3-axis digital compass

GY-GPS6MV2

myBlox NEO-6M-0-001 over UART


Later I will also add a PWM beeper and a OLED display. To make it a fully working backup variometer.
For easy powering I use a USB backup charger. The ESP32 I am using is a AZDelivery ESP32 NodeMCU clone.

I will try to keep the UART of the ESP32 free so that we will be able to use this as an USB gadget device.

A wired connection should have less latency. But because the refresh rate of an ebook reader is bad anyways
it should not harm to use the Wifi link if your ebook reader has no USB gadget or you prefer to have it wireless.

On the ebook reader side there will be an Android sensor driver for each of the sensors. Therefore all Android
Apps which make use of any of the sensors will work without modification. My target is to get XC Soar and XC Track
running. And later write a free XC Track replacement which is optimized for the ebook readers.

Also because of the ESP32 has a bluetooth frontend I will try to later add this connection. This will make it easy to connect
it to your smartphone.

Any help is of course very welcome

Happy hacking :)

How to get it working NOW!

Export your IDF_PATH and toolchain path as usual e.g.

NOTE: currently only the stable release/v4.1 branch of the esp-idf is supported.

```sh
export $IDF_PATH=$HOME/path/to/esp-idf
source $IDF_PATH/export.sh
```

Clone this repository with:

```sh
git clone --recursive https://gitlab.com/mrnice/free-fly-esp32.git
```

Build the project with

```sh
idf.py build
```

If your ESP32 is connected at /dev/ttyUSB0 you can use:

```sh
idf.py -p /dev/ttyUSB0 flash
```

To flash the free fly esp32 to your ESP32.

The free fly esp32 output should be something like (for now):

```sh
$ socat /dev/ttyUSB0,b115200 STDOUT
$POV,T,0.23*02
$POV,P,964.36*09
$POV,AY:50.3*5F
$POV,AP:-4.9*40
$POV,AR:-0.9*46
$POV,MX:250.24*55
$POV,MY:607.20*56
$POV,MZ:-1016.60*4B
$POV,T,0.23*02
$POV,P,964.34*0B
...
```
