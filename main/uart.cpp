/*
 * Free fly ESP project
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 */

#include "include/uart.h"

#include <string.h>
#include <fcntl.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#include "driver/uart.h"

#include "include/common.h"

#define GPS_RXD (4)
#define BUF_SIZE (2048)

////////////////////////////////////////////////////////////
//// Public
////////////////////////////////////////////////////////////

void task_initUART(void *ignore)
{

    uart_config_t uart_config0 =
    {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };

    uart_param_config(UART_NUM_1, &uart_config0);
    uart_set_pin(UART_NUM_1, UART_PIN_NO_CHANGE, GPS_RXD, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM_1, BUF_SIZE * 2, 0, 0, NULL, 0);

    vTaskDelete(NULL);
}
