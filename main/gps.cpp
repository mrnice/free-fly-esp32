/*
 * Free fly ESP project
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 */

#include "include/gps.h"

#include <string.h>
#include <fcntl.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include "driver/uart.h"

#include "include/open-vario.h"
#include "include/common.h"

QueueHandle_t nmea_messages_queue = NULL;

////////////////////////////////////////////////////////////
//// Public
////////////////////////////////////////////////////////////

void task_send_gps(void *p)
{
    char buffer[OUTPUT_DATA_SIZE];

    while (true)
    {
        if(xQueueReceive(nmea_messages_queue, &buffer, 10/portTICK_PERIOD_MS))
        {
            printf("%s", buffer);
        }
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

void task_read_gps(void *p)
{
    printf("GPS_TASK\n");

    int pos;                              ///< last position of '\n'
    char* line_end;                       ///< pointer to the last occurrence of '\n'
    int len;                              ///< current number of bytes read from the serial device
    size_t buffer_len;                    ///< serial device current fifo data size
    char output_buffer[OUTPUT_DATA_SIZE]; ///< buffer to send, this contains at least one line
    char old_buffer[OUTPUT_DATA_SIZE];    ///< the old buffer, used to combine n buffers into at least one line
    char read_buffer[OUTPUT_DATA_SIZE];   ///< buffer to read from the serial device

    // Terminate strings
    output_buffer[0]='\0';
    old_buffer[0]='\0';
    read_buffer[0]='\0';

    nmea_messages_queue = xQueueCreate(20, sizeof( output_buffer ));

    while (true)
    {
        len = uart_get_buffered_data_len(UART_NUM_1, &buffer_len);
        buffer_len = buffer_len<OUTPUT_DATA_SIZE ? buffer_len : OUTPUT_DATA_SIZE;
        len = uart_read_bytes(UART_NUM_1, (uint8_t*)read_buffer, buffer_len, 0);
        if(len >0)
        {
            read_buffer[len]='\0';
            strncat(old_buffer, read_buffer, OUTPUT_DATA_SIZE-1);
            line_end = strrchr(old_buffer, '\n');
            if(line_end != NULL)
            {
                pos = line_end - old_buffer;
                strncpy(output_buffer, old_buffer, pos);
                output_buffer[pos]='\0';
                strncpy(old_buffer, line_end, OUTPUT_DATA_SIZE-1);
                xQueueSend(nmea_messages_queue, output_buffer, 10/portTICK_PERIOD_MS);
                output_buffer[0]='\0';
            }
        }
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}
