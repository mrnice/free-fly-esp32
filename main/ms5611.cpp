/*
 * Free Fly ESP32
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#include "include/ms5611.h"

#include <string.h>
#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <ms5611.h>

#include "include/common.h"
#include "include/open-vario.h"

#define I2C_ADDR MS5611_ADDR_CSB_LOW
#define I2C_PORT 0
#define OVERSAMPLING_RATIO MS5611_OSR_4096

void task_ms561101ba30(void*)
{
    char buffer[OUTPUT_DATA_SIZE];

    ms5611_t device;
    memset(&device, 0, sizeof(ms5611_t));

    ESP_ERROR_CHECK(ms5611_init_desc(&device, I2C_ADDR, I2C_PORT, SDA_GPIO, SCL_GPIO));
    ESP_ERROR_CHECK(ms5611_init(&device, OVERSAMPLING_RATIO));

    float temperature;
    int32_t pressure;

    while (true)
    {
        if (ms5611_get_sensor_data(&device, &pressure, &temperature) != ESP_OK)
        {
            printf("Error reading sensor data from device");
            continue;
        }

        // For now we use the raw data to develop the filter.
        int res = get_nmea_pov_t(buffer, OUTPUT_DATA_SIZE, temperature);
        if(res > -1)
        {
            xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
        }
        res = get_nmea_pov_p(buffer, OUTPUT_DATA_SIZE, pressure);
        if(res > -1)
        {
            xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
        }
        vTaskDelay(100/portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}
