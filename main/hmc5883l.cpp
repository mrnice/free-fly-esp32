/*
 * Free Fly ESP32
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#include "include/hmc5883l.h"

#include <stdio.h>
#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#include <hmc5883l.h>

#include "include/common.h"
#include "include/open-vario.h"

void task_hmc5883l(void*)
{
    char buffer[OUTPUT_DATA_SIZE];

    hmc5883l_dev_t device;
    memset(&device, 0, sizeof(hmc5883l_dev_t));

    ESP_ERROR_CHECK(hmc5883l_init_desc(&device, 0, SDA_GPIO, SCL_GPIO));
    while (hmc5883l_init(&device) != ESP_OK)
    {
        printf("Device not found hmc5883l\n");
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }

    hmc5883l_set_opmode(&device, HMC5883L_MODE_CONTINUOUS);
    hmc5883l_set_samples_averaged(&device, HMC5883L_SAMPLES_8);
    hmc5883l_set_data_rate(&device, HMC5883L_DATA_RATE_07_50);
    hmc5883l_set_gain(&device, HMC5883L_GAIN_1090);

    while (true)
    {
        hmc5883l_data_t data;
        if (!hmc5883l_get_data(&device, &data) == ESP_OK) {
            vTaskDelay(100/portTICK_PERIOD_MS);
            continue;
        }

        // For now we use the raw data to develop the filter.
        int res = get_nmea_pov_mx(buffer, OUTPUT_DATA_SIZE, data.x);
        if(res > -1)
        {
            xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
        }
        res = get_nmea_pov_my(buffer, OUTPUT_DATA_SIZE, data.y);
        if(res > -1)
        {
            xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
        }
        res = get_nmea_pov_mz(buffer, OUTPUT_DATA_SIZE, data.z);
        if(res > -1)
        {
            xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
        }
        vTaskDelay(100/portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}
