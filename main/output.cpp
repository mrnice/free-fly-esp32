/*
 * Free Fly ESP32
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#include "include/output.h"

#include <string.h>
#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#include "include/common.h"

void task_output(void *p)
{
    char buffer[OUTPUT_DATA_SIZE];

    while (true)
    {
        if(xQueueReceive(simple_global_xQueueHandle, &buffer[0], 10/portTICK_PERIOD_MS))
        {
            printf("%s", buffer);
        }
        vTaskDelay(1/portTICK_PERIOD_MS);
    }
}
