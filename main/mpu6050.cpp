/*
 * Free Fly ESP32
 *
 * Original Copyright
 * Display.c
 *
 *  Created on: 14.08.2017
 *      Author: darek
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 * BSD Licensed as described in the file LICENSE
 */

#include "include/mpu6050.h"

#include <driver/i2c.h>
#include <esp_log.h>
#include <esp_err.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>

#include <MPU6050.h>
#include <MPU6050_6Axis_MotionApps20.h>

#include "include/common.h"
#include "include/open-vario.h"

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
uint16_t packetSize = 42;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU

extern xSemaphoreHandle mpu6050_i2c_forwardign_ready;

void task_mpu6050(void*)
{
    char buffer[OUTPUT_DATA_SIZE];

    MPU6050 mpu = MPU6050();
    mpu.initialize();
    mpu.dmpInitialize();

    // This need to be setup individually
    mpu.setXGyroOffset(220);
    mpu.setYGyroOffset(76);
    mpu.setZGyroOffset(-85);
    mpu.setZAccelOffset(1788);

    mpu.setDMPEnabled(true);

    mpu.setI2CBypassEnabled(true);
    xSemaphoreGive(mpu6050_i2c_forwardign_ready);

    while(1)
    {
        mpuIntStatus = mpu.getIntStatus();
        // get current FIFO count
        fifoCount = mpu.getFIFOCount();

        if ((mpuIntStatus & 0x10) || fifoCount == 1024)
        {
            // reset so we can continue cleanly
            mpu.resetFIFO();

            // otherwise, check for DMP data ready interrupt frequently)
        }
        else if (mpuIntStatus & 0x02)
        {
            // wait for correct available data length, should be a VERY short wait
            while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

            // read a packet from FIFO

            mpu.getFIFOBytes(fifoBuffer, packetSize);
            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetGravity(&gravity, &q);
            mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

            // For now we use the raw data to develop the filter.
            int res = get_nmea_pov_ay(buffer, OUTPUT_DATA_SIZE, ypr[0] * 180/M_PI);
            if(res > -1)
            {
                xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
            }
            res = get_nmea_pov_ap(buffer, OUTPUT_DATA_SIZE, ypr[1] * 180/M_PI);
            if(res > -1)
            {
                xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
            }
            res = get_nmea_pov_ar(buffer, OUTPUT_DATA_SIZE, ypr[2] * 180/M_PI);
            if(res > -1)
            {
                xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
            }
        }

        //Best result is to match with DMP refresh rate
        // Its last value in components/MPU6050/MPU6050_6Axis_MotionApps20.h file line 310
        // Now its 0x13, which means DMP is refreshed with 10Hz rate
        vTaskDelay(100/portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}
