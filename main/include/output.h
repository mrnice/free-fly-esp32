/*
 * Free Fly ESP32
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef OUTPUT_H_
#define OUTPUT_H_

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * task output
 */
void task_output(void *ignore);

#ifdef __cplusplus
}
#endif

#endif /* OUTPUT_H_ */
