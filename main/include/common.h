/*
 * Free Fly ESP32
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <driver/i2c.h>

#define SDA_GPIO (gpio_num_t)19
#define SCL_GPIO (gpio_num_t)18
#define OUTPUT_DATA_SIZE 400

#ifdef __cplusplus
extern "C"
{
#endif

extern QueueHandle_t simple_global_xQueueHandle;
extern QueueHandle_t simple_global_xQueueHandle2;

#ifdef __cplusplus
}
#endif

#endif /* COMMON_H_ */
