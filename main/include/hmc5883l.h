/*
 * Free Fly ESP32
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef HMC5883l_H_
#define HMC5883l_H_

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * task hmc5883l
 */
void task_hmc5883l(void *ignore);

#ifdef __cplusplus
}
#endif

#endif /* HMC5883l_H_ */
