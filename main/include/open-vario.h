/*
 * Free fly ESP project
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 * We implement the open vario POV ptrotocol.
 * The specification can be found here:
 * * https://github.com/Turbo87/openvario-protocol
 *
 * It is an NMEA like string
 * $POV,<type>,<value>,<type>,<value>,...*<checksum>
 */

#ifndef OPEN_VARIO_H_
#define OPEN_VARIO_H_

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define NMEA_MINIMAL_BUFFER_LENGTH 22
#define NMEA_CHECKSUM_LENGTH 6
#define NMEA_DATA_SIZE 80

/**
 * Get NMEA string POV T
 *
 * T: temperature in °C
 * Example: $POV,T,23.52*35
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param t temperature in C with 0.01 C resulution
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_t(char * buf, size_t buf_max_len, int32_t t);

/**
 * Get NMEA string POV P
 *
 * P: static pressure in hPa
 * Example: $POV,P,1018.35*39
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param p pressure in mbar with 0.01 mbar resolution
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_p(char * buf, size_t buf_max_len, int32_t p);

/**
 * Get NMEA string POV E
 *
 * E: TE vario in m/s
 * Example: $POV,E,2.15*14
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param m_s vario in m/s
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_e(char * buf, size_t buf_max_len, float m_s);

/**
 * Get NMEA string POV MX
 *
 * MX: magnetic data x in mG
 * Example: $POV,MX:402.04*56
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param m_s vario in m/s
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_mx(char * buf, size_t buf_max_len, float x);

/**
 * Get NMEA string POV MY
 *
 * MY: magnetic data y in mG
 * Example: $POV,MY:-553.84*77
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param m_s vario in m/s
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_my(char * buf, size_t buf_max_len, float y);

/**
 * Get NMEA string POV MZ
 *
 * MZ: magnetic data z in mG
 * Example: $POV,MZ:-734.16*7C
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param m_s vario in m/s
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_mz(char * buf, size_t buf_max_len, float z);

/**
 * Get NMEA string POV AY
 *
 * AY: acceleration yaw
 * Example: $POV,AY:53.2*5D
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param yaw
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_ay(char * buf, size_t buf_max_len, float yaw);

/**
 * Get NMEA string POV AP
 *
 * AP: acceleration pitch
 * Example: $POV,AP:-6.3*48
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param pitch
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_ap(char * buf, size_t buf_max_len, float pitch);

/**
 * Get NMEA string POV AR
 *
 * AR: acceleration roll
 * Example: $POV,AR:-0.3*4C
 *
 * @param buf pointer to an allocated buffer
 * @param buf_max_len maximal size of the buffer
 * @param roll
 * @returns lenght of the string -1 on failure
 */
int get_nmea_pov_ar(char * buf, size_t buf_max_len, float roll);

#ifdef __cplusplus
}
#endif

#endif /* OPEN_VARIO_H_ */
