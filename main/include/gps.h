/*
 * Free fly ESP project
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef GPS_H_
#define GPS_H_

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * GPS task
 *
 * @param p
 */
void task_send_gps(void *p);

void task_read_gps(void *p);

#ifdef __cplusplus
}
#endif

#endif /* GPS_H_ */
