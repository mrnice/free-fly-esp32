/*
 * Free fly ESP project
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef UART_H_
#define UART_H_

#ifdef __cplusplus
extern "C"
{
#endif

void task_initUART(void *ignore);

#ifdef __cplusplus
}
#endif

#endif /* UART_H_ */
