/*
 * Free Fly ESP32
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <freertos/semphr.h>

#include "sdkconfig.h"
#include "i2cdev.h"

#include "include/uart.h"
#include "include/hmc5883l.h"
#include "include/ms5611.h"
#include "include/mpu6050.h"
#include "include/gps.h"
#include "include/output.h"
#include "include/common.h"

extern "C" {
    void app_main(void);
}

char buffer[OUTPUT_DATA_SIZE];
QueueHandle_t simple_global_xQueueHandle = NULL;
xSemaphoreHandle mpu6050_i2c_forwardign_ready = NULL;

void app_main(void)
{
    simple_global_xQueueHandle = xQueueCreate(20, sizeof( buffer ));
    vSemaphoreCreateBinary(mpu6050_i2c_forwardign_ready);

    ESP_ERROR_CHECK(i2cdev_init());
    vTaskDelay(500/portTICK_PERIOD_MS);

    xTaskCreate(&task_mpu6050, "mpu6050_task", 8192, NULL, 5, NULL);
    xTaskCreate(&task_ms561101ba30, "ms561101ba30_task", 8192, NULL, 5, NULL);
    xTaskCreate(&task_output, "output_task", 8192, NULL, 5, NULL);

    if(xSemaphoreTake(mpu6050_i2c_forwardign_ready, portMAX_DELAY))
    {
        xTaskCreate(&task_hmc5883l, "hmc5883l_task", 8192, NULL, 5, NULL);
    }

    vTaskDelay(500/portTICK_PERIOD_MS);

    xTaskCreate(&task_initUART, "uart_init_task", 2048, NULL, 5, NULL);
    vTaskDelay(500/portTICK_PERIOD_MS);

    xTaskCreate(&task_read_gps, "gps_read_task", 18192, NULL, 5, NULL);
    vTaskDelay(500/portTICK_PERIOD_MS);

    xTaskCreate(&task_send_gps, "gps_send_task", 18192, NULL, 5, NULL);

}
