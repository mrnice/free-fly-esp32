/*
 * Free fly ESP project
 *
 * Copyright (C) 2020 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 */

#include "include/open-vario.h"

////////////////////////////////////////////////////////////
//// Private helper functions NMEA
////////////////////////////////////////////////////////////

/**
 * Calculate NMEA checksum for the given string
 *
 * @param buf pointer to string
 * @param len length of sting
 */
static inline char get_checksum(char * buf, size_t len)
{
    char c = 0;
    for (int i = 0; i < len; i++)
    {
        c = c ^ buf[i];
    }
    return c;
}

/**
 * Add a checksum, \r, \n and null termination to a given NMEA string
 *
 * @param buf pointer to string
 * @param len length of sting
 * @param max_len maximum length of sting, must be at least NMEA_CHECKSUM_LENGTH greater then len
 */
static inline int add_checksum(char * buf, size_t len, size_t max_len)
{
    if (len < 2)
        return -1;

    if (max_len < len + NMEA_CHECKSUM_LENGTH)
        return -1;

    char c = get_checksum(&buf[1], len);
    char c_buf[3];

    int ret = snprintf(c_buf, 3, "%02X",c);
    if (ret<0)
        return ret;

    buf[len++] = '*';
    buf[len++] = c_buf[0];
    buf[len++] = c_buf[1];
    buf[len++] = '\r';
    buf[len++] = '\n';
    buf[len] = '\0';

    return len;
}

////////////////////////////////////////////////////////////
//// Public
////////////////////////////////////////////////////////////

int get_nmea_pov_t(char * buf, size_t buf_max_len, int32_t t)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;
    int32_t a = t/100;
    int32_t b = t%100;
    int len;
    if (b<0)
    {
        b=b*-1;
        if (a==0)
        {
            len = snprintf(buf, buf_max_len, "$POV,T,-%d.%02d",a,b);
            return len;
        }
    }
    len = snprintf(buf, buf_max_len, "$POV,T,%d.%02d",a,b);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}

int get_nmea_pov_p(char * buf, size_t buf_max_len, int32_t p)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;

    int32_t a = p/100;
    int32_t b = p%100;
    int len;
    if (b<0)
    {
        b=b*-1;
        if (a==0)
        {
            len = snprintf(buf, buf_max_len, "$POV,P,-%d.%02d",a,b);
            return len;
        }
    }
    len = snprintf(buf, buf_max_len, "$POV,P,%d.%02d",a,b);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}

int get_nmea_pov_e(char * buf, size_t buf_max_len, float m_s)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;
    int len;
    if (m_s > -0.01 && m_s < 0)
        m_s = 0.0;
    len = snprintf(buf, buf_max_len, "$POV,E,%2.2f", m_s);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}

int get_nmea_pov_mx(char * buf, size_t buf_max_len, float x)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;
    int len;
    if (x > -0.01 && x < 0)
        x = 0.0;
    len = snprintf(buf, buf_max_len, "$POV,MX:%.2f", x);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}

int get_nmea_pov_my(char * buf, size_t buf_max_len, float y)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;
    int len;
    if (y > -0.01 && y < 0)
        y = 0.0;
    len = snprintf(buf, buf_max_len, "$POV,MY:%.2f", y);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}

int get_nmea_pov_mz(char * buf, size_t buf_max_len, float z)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;
    int len;
    if (z > -0.01 && z < 0)
        z = 0.0;
    len = snprintf(buf, buf_max_len, "$POV,MZ:%.2f", z);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}

int get_nmea_pov_ay(char * buf, size_t buf_max_len, float ay)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;
    int len;
    if (ay > -0.01 && ay < 0)
        ay = 0.0;
    len = snprintf(buf, buf_max_len, "$POV,AY:%3.1f", ay);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}

int get_nmea_pov_ap(char * buf, size_t buf_max_len, float ap)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;
    int len;
    if (ap > -0.01 && ap < 0)
        ap = 0.0;
    len = snprintf(buf, buf_max_len, "$POV,AP:%3.1f", ap);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}

int get_nmea_pov_ar(char * buf, size_t buf_max_len, float ar)
{
    if(buf_max_len < NMEA_MINIMAL_BUFFER_LENGTH-NMEA_CHECKSUM_LENGTH)
        return -1;
    int len;
    if (ar > -0.01 && ar < 0)
        ar = 0.0;
    len = snprintf(buf, buf_max_len, "$POV,AR:%3.1f", ar);

    if(add_checksum(buf, len, buf_max_len) < 0)
        return -1;

    return len;
}